NAME = ft_traceroute

FLAG = -Wall -Wextra -Werror -D_GNU_SOURCE 

OS_NAME := $(shell uname)
ifeq ($(OS_NAME),Darwin)
	FLAG += -D __MAC__
endif

HEADER = includes/ft_traceroute.h includes/inet.h
INCLUDES = -I includes/

SRCS_PATH = srcs/
UTILS_PATH = utils/
SRCS_NAME = debug.c main.c parsing.c ft_traceroute.c packet.c ft_socket.c print.c
UTILS_NAME = ft_strcmp.c ft_memcpy.c ft_memset.c time.c ft_memalloc.c \
				listlen.c str_is_number.c ft_atoi.c ft_isdigit.c ft_isalpha.c \
				ft_isblank.c ft_strlen.c
UTILS = $(addprefix $(UTILS_PATH), $(UTILS_NAME))
SRCS_NAME += $(UTILS)
SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(FLAG) $(INCLUDES) -o $(NAME) $(OBJS)

$(OBJS_PATH)%.o:$(SRCS_PATH)%.c $(HEADER)
	mkdir -p $(OBJS_PATH)
	mkdir -p $(addprefix $(OBJS_PATH), $(UTILS_PATH))
	$(CC) $(FLAG) $(INCLUDES) -o $@ -c $<

clean:
	$(RM) -rf $(OBJS_PATH)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
