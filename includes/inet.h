#ifndef INET_H
#define INET_H

#include <stdint.h>

#ifdef __MAC__

#define ICMP_DEST_UNREACH 3 // ICMP type 3
#define ICMP_NET_UNREACH 0 // network unreachable !N
#define ICMP_HOST_UNREACH 1 // host unreachable !H
#define ICMP_PROT_UNREACH 2 // Protocol Unreachable !P
#define ICMP_PORT_UNREACH 3 // destination unreachable 
#define ICMP_SR_FAILED 5 // Source Route failed !S
#define ICMP_NET_UNKNOWN 6 // destination network unknown !U
#define ICMP_HOST_UNKNOWN 7 // destination host unknown !W
#define ICMP_HOST_ISOLATED 8 // source isolated !Z
#define ICMP_NET_ANO 9 // !I
#define ICMP_HOST_ANO 10 // !A
#define ICMP_NET_UNR_TOS 11 // !Q
#define ICMP_HOST_UNR_TOS 12 // !T
#define ICMP_PKT_FILTERED 13 // packet filtered !X 
#define ICMP_PREC_VIOLATION 14 // Prohibited !V
#define ICMP_PREC_CUTOFF 15 // Precedence cut off !C

#define ICMP_TIME_EXCEEDED 11 // ICMP type 11
#define ICMP_EXC_TTL 0 // TTL exceed

struct	icmphdr
{
	u_int8_t	type;
	u_int8_t	code;
	u_int16_t	checksum;
	u_int16_t	id;
	u_int16_t	sequence;
};

struct iphdr {
	u_int8_t	ihl:4;
	u_int8_t	version:4;
	u_int8_t	tos;
	u_int16_t	tot_len;
	u_int16_t	id;
	u_int16_t	frag_off;
	u_int8_t	ttl;
	u_int8_t	protocol;
	u_int16_t	check;
	u_int32_t	saddr;
	u_int32_t	daddr;
};
#endif

#define IPV4 4

#define PACKET_SIZE 24

#define DEFAULT_HOPS 64
#define DEFAULT_TIMEOUT 3
#define DEFAULT_NB_PROBES 3

#define SRC_PORT 45312 //51238
#define DST_PORT 33435

#define MAX_HOPS 255
#define MAX_TIMEOUT 100
#define MAX_PROBES INT_MAX


#endif