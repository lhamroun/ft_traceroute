#ifndef FT_TRACEROUTE_H
#define FT_TRACEROUTE_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <netinet/in.h>

#include "inet.h"

#define TTL_EXCEED_CODE 0
#define TTL_EXCEED_TYPE 11

#define DST_UNREACHABLE_CODE 3
#define DST_UNREACHABLE_TYPE 3

#define PKT_FILTERED_CODE 13
#define PKT_FILTERED_TYPE 3

enum                e_type
{
    ICMP = 1,
    UDP = 17
};

// for bonus
typedef struct      s_options
{
    bool            helper;
    bool            verbose;
    uint8_t         hops_max;
    uint8_t         nb_probe;
    uint8_t         time;
    uint16_t        sport;
    uint16_t        dport;
    enum e_type     type; // pas sure que je fasse ICMP ?
}                   t_options;

// this struct count options for parsing
typedef struct      s_options_check
{
    int             nhelper;
    int             nverbose;
    int             nhops_max;
    int             nnb_probe;
    int             ntime;
    int             ndport;
    int             nnb_ip;
}                   t_options_check;

typedef struct      s_args
{
    char                *ip;
    struct addrinfo     *addr;
    struct s_options    opt;
}                   t_args;

typedef struct      s_timestamp
{
    uint16_t        dport;
    uint32_t        start;
    uint32_t        end;
}                   t_timestamp;

t_args          parsing(int ac, char **av);
void            ft_traceroute(t_args args);
char        	*resolve_dns(struct addrinfo *res);
struct addrinfo *is_ip_addr(char *addr);
int             check_ip_hdr(struct iphdr *ip);
int             check_icmp_hdr(struct icmphdr *icmp);
unsigned short	checksum(void *b, int len);
int             create_recv_socket(t_options opt);
int             create_send_socket(void);
void            print_initial_state(t_args args, char *ipv4);
void            print_ttl_stats(void *buf);
void            print_unreachable(int val);

void            debug_args(t_args args);
void            debug_addrinfo(struct addrinfo *addr);
void            debug_icmp(struct icmphdr *icmp);
void            debug_ip(struct iphdr *ip);
void            debug_udp(struct udphdr *udp);
void            debug_sockaddr(struct sockaddr_in *addr);
void            debug_options(t_options opt);
void            debug_options_check(t_options_check opt_check);
int             set_udp_socket(int fd, int ttl);
void            debug_network_address(uint32_t ip);

/**
**  Utils functions
**/
void			ft_usleep(unsigned int duration);
uint64_t	    get_time_now(void);
uint64_t    	timeval_to_ul(struct timeval tv);
int		        ft_strcmp(char const *s1, char const *s2);
void	        *ft_memset(void *s, int c, size_t n);
void	        *ft_memcpy(void *dest, void const *src, size_t size);
void            *ft_memalloc(size_t size);
int     		listlen(char **tab);
int     		str_is_number(char *str);
int     		ft_atoi(char const *str);
int     		ft_isdigit(int c);
int     		ft_isblank(char c);
int		        ft_isalpha(int c);
size_t		    ft_strlen(char const *s);

#endif