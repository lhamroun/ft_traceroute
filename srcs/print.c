#include "ft_traceroute.h"

extern t_timestamp  timestamp;
extern bool         print_server_ip;
extern uint32_t     nb_packet;

void    print_unreachable(int val)
{
    switch (val)
    {
        case -1:
            printf(" !H ");
            break ;
        case -2:
            printf(" !N ");
            break ;
        case -3:
            printf(" !P ");
            break ;
        case -4:
            printf(" !S ");
            break ;
        case -5:
            printf(" !U ");
            break ;
        case -6:
            printf(" !W ");
            break ;
        case -7:
            printf(" !I ");
            break ;
        case -8:
            printf(" !A ");
            break ;
        case -9:
            printf(" !Z ");
            break ;
        case -10:
            printf(" !Q ");
            break ;
        case -11:
            printf(" !T ");
            break ;
        case -12:
            printf(" !X ");
            break ;
        case -13:
            printf(" !V ");
            break ;
        case -14:
            printf(" !C ");
            break ;
        default :
            printf(" !%d ", -1 * val);
    }
}

void    print_ttl_stats(void *buf)
{
    char            *server_name;
    struct iphdr    *ip;
    struct addrinfo *addr;
    char            host_buf[NI_MAXHOST];

    server_name = NULL;
    ip = (struct iphdr *)buf;
    ft_memset(host_buf, 0, sizeof(host_buf));
    if (!(server_name = ft_memalloc(sizeof(char) * INET_ADDRSTRLEN)))
	{
        printf(" * ");
        return ;
    }
    if (!inet_ntop(AF_INET, &ip->saddr, server_name, INET_ADDRSTRLEN))
    {
        printf(" * ");
        free(server_name);
        return ;
    }
    addr = is_ip_addr(server_name);
    if (addr && addr->ai_addr)
        getnameinfo(addr->ai_addr, INET_ADDRSTRLEN, host_buf, sizeof(host_buf), NULL, 0, NI_NAMEREQD);
        //host_buf[0] = 0;
    if (!print_server_ip)
    {
        printf("%s (%u.%u.%u.%u) %.3f ms ",
            ft_strlen(host_buf) == 0 ? "" : host_buf,
            ( 0x000000ff & ip->saddr),
            ((0x0000ff00 & ip->saddr) >> 8),
            ((0x00ff0000 & ip->saddr) >> 16),
            ((0xff000000 & ip->saddr) >> 24),
            (float)(timestamp.end - timestamp.start) / 1000);
        print_server_ip = true;
    }
    else
        printf(" %.3f ms ", (float)(timestamp.end - timestamp.start) / 1000);
    free(server_name);
}

void    print_initial_state(t_args args, char *ipv4)
{
    printf("traceroute to %s (%s), %d hops max, %zu byte packets\n",
        args.ip,
        ipv4,
        args.opt.hops_max,
        sizeof(struct iphdr) + sizeof(struct udphdr) + PACKET_SIZE);
}
