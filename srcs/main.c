#include "ft_traceroute.h"

void    usage(void)
{
    printf("./ft_traceroute <ip> [--help]\n");
}

int     main(int ac, char **av)
{
    t_args  args;

    if (getuid())
	{
		printf("You need to be root to run ./ft_traceroute\n");
		return EXIT_SUCCESS;
	}
    args = parsing(ac - 1, av + 1);
    if (args.opt.helper || !args.ip)
    {
        usage();
        return EXIT_SUCCESS;
    }
    ft_traceroute(args);
    freeaddrinfo(args.addr);
    return EXIT_SUCCESS;
}