#include "ft_traceroute.h"

void    debug_args(t_args args)
{
    printf("arguments :\n");
    printf("ip : %s\n", args.ip);
    debug_addrinfo(args.addr);
    debug_options(args.opt);
    printf("\n\n");
}

void    debug_options(t_options opt)
{
    printf("options checker :\n");
    printf("option help       %d\n", opt.helper);
    printf("option verbose    %d\n", opt.verbose);
    printf("option hops max   %d\n", opt.hops_max);
    printf("option nb probe   %d\n", opt.nb_probe);
    printf("option ntime      %d\n", opt.time);
    printf("option sport      %d\n", opt.sport);
    printf("option dport      %d\n", opt.dport);
    printf("option type       %s\n", opt.type == UDP ? "UDP" : opt.type == ICMP ? "ICMP" : "OTHER PROTOCOL");
}

void    debug_options_check(t_options_check opt_check)
{
    printf("options checker :\n");
    printf("option check help       %d\n", opt_check.nhelper);
    printf("option check verbose    %d\n", opt_check.nverbose);
    printf("option check hops max   %d\n", opt_check.nhops_max);
    printf("option check nb probe   %d\n", opt_check.nnb_probe);
    printf("option check ntime      %d\n", opt_check.ntime);
    printf("option check dport      %d\n", opt_check.ndport);
    printf("option check ip addr    %d\n", opt_check.nnb_ip);
}

void    debug_addrinfo(struct addrinfo *addr)
{
    printf("addr infos :\n");
    printf("    ai_flags        --> %d\n", addr->ai_flags);
    printf("    ai_family       --> %s\n", addr->ai_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    ai_socktype     --> %s\n", addr->ai_socktype == SOCK_RAW ? "SOCK_RAW" : addr->ai_socktype == SOCK_DGRAM ? "SOCK_DGRAM" : "OTHER SOCK TYPE");
    printf("    ai_protocole    --> %d\n", addr->ai_protocol);
    printf("    ai_addrlen      --> %d\n", addr->ai_addrlen);
#ifdef __MAC__
    printf("    ai_addr len     --> %d\n", addr->ai_addr->sa_len);
#endif
    printf("    ai_addr fam     --> %d\n", addr->ai_addr->sa_family);
    printf("    ai_addr data    --> %s\n", addr->ai_addr->sa_data);
    printf("    ai_canonname    --> %s\n", addr->ai_canonname);
    printf("    ai_next         --> %p\n", addr->ai_next);
    printf("\n\n");
}

void    debug_sockaddr(struct sockaddr_in *addr)
{
    printf("sockaddr_in :\n");
    printf("    sin_family      --> %s\n", addr->sin_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    sin_port        --> %hu\n", ntohs(addr->sin_port));
    printf("    s_addr          --> %u.%u.%u.%u\n", (addr->sin_addr.s_addr & 0x000000ff), ((addr->sin_addr.s_addr & 0x0000ff00) >> 8), ((addr->sin_addr.s_addr & 0x00ff0000) >> 16), ((addr->sin_addr.s_addr & 0xff000000) >> 24));
    printf("    sin_zero        --> %s\n", addr->sin_zero);
    printf("\n\n");
}

void    debug_ip(struct iphdr *ip)
{
    printf("IP packet contains :\n");
    printf("ihl         : %hhu\n", ip->ihl);
    printf("version     : %hhu\n", ip->version);
    printf("tos         : %hhu\n", ip->tos);
    printf("tot_len     : %hu\n", ip->tot_len);
    printf("id          : %hu\n", ip->id);
    printf("frag_off    : %hu\n", ip->frag_off);
    printf("ttl         : %hhu\n", ip->ttl);
    printf("protocol    : %s\n", ip->protocol == 1 ? "ICMP" : ip->protocol == 17 ? "UDP" : "OTHER PROTOCOL");
    printf("check       : 0x%hx\n", ip->check);
    printf("saddr       : %u.%u.%u.%u\n",
        ( 0x000000ff & ip->saddr),
        ((0x0000ff00 & ip->saddr) >> 8),
        ((0x00ff0000 & ip->saddr) >> 16),
        ((0xff000000 & ip->saddr) >> 24));
    printf("daddr       : %u.%u.%u.%u\n",
        ( 0x000000ff & ip->daddr),
        ((0x0000ff00 & ip->daddr) >> 8),
        ((0x00ff0000 & ip->daddr) >> 16),
        ((0xff000000 & ip->daddr) >> 24));
    printf("\n\n");
}

void    debug_icmp(struct icmphdr *icmp)
{
    printf("ICMP packet contains :\n");
    printf("type        : %u\n", icmp->type);
    printf("code        : %u\n", icmp->code);
#ifdef __MAC__
    printf("sequence    : %u\n", icmp->sequence);
#endif
    printf("checksum    : 0x%x\n", icmp->checksum);
#ifdef __MAC__
    printf("id          : %u\n", icmp->id);
#endif
    char *tmp = (char *)icmp + sizeof(struct icmphdr);
    printf("    data    : %s\n", tmp);
    printf("\n\n");
}

void    debug_udp(struct udphdr *udp)
{
    printf("UDP packet contains :\n");
    printf("sport       : %hu\n", ntohs(udp->uh_sport));
    printf("dport       : %hu\n", ntohs(udp->uh_dport));
    printf("len         : %u\n", udp->uh_ulen);
    printf("chksum      : 0x%x\n", udp->uh_sum);
    char *tmp = (char *)udp + sizeof(struct udphdr);
    printf("    data    : %s\n", tmp);
    printf("\n\n");
}

void    debug_network_address(uint32_t ip)
{
    printf("network @   : %u.%u.%u.%u\n",
        ( 0x000000ff & ip),
        ((0x0000ff00 & ip) >> 8),
        ((0x00ff0000 & ip) >> 16),
        ((0xff000000 & ip) >> 24));
}
