#include "ft_traceroute.h"

void	*ft_memalloc(size_t size)
{
	void	*buf;

	buf = NULL;
	if (!(buf = (void *)malloc(size)))
		return (NULL);
	return (ft_memset(buf, 0, size));
}
