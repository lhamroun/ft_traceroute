#include "ft_traceroute.h"

void			ft_usleep(unsigned int duration)
{
	uint64_t	start;

	start = get_time_now();
	while (get_time_now() - start < (uint64_t)duration)
	{
	}
}

uint64_t	get_time_now(void)
{
	struct timeval	timestamp;

	gettimeofday(&timestamp, NULL);
	return ((uint64_t)timestamp.tv_sec * 1000000 + (uint64_t)timestamp.tv_usec);
}

uint64_t	timeval_to_ul(struct timeval tv)
{
	return ((uint64_t)tv.tv_sec * 1000000 + (uint64_t)tv.tv_usec);
}
