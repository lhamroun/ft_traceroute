#include "ft_traceroute.h"

void	*ft_memset(void *s, int c, size_t n)
{
	char	*cpy;

	cpy = (char *)s;
	while (n > 0)
	{
		*cpy++ = (unsigned char)c;
		n--;
	}
	return (s);
}
