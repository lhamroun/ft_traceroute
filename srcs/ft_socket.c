#include "ft_traceroute.h"

int     create_recv_socket(t_options opt)
{
    int				fd_socket;
    int				res;
    struct timeval  time;

    res = 0;
    time.tv_sec = opt.time;
    time.tv_usec = 0;
	fd_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	res = setsockopt(fd_socket, SOL_SOCKET, SO_RCVTIMEO, &time, sizeof(struct timeval));
    return res ? -1 : fd_socket;
}

int     create_send_socket(void)
{
    int				fd_socket;

	fd_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	return fd_socket;
}

int     set_udp_socket(int fd, int ttl)
{
	return setsockopt(fd, IPPROTO_IP, IP_TTL, &ttl, sizeof(int));
}
