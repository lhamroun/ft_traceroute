#include "ft_traceroute.h"

bool        finish = false;
t_timestamp timestamp = {.start=0, .end=0, .dport=0};
bool        print_server_ip = false;
uint32_t    nb_packet = 0;

char	*resolve_dns(struct addrinfo *res)
{
	char				*addr;
	struct sockaddr_in	*tmp;

	if (!(addr = ft_memalloc(sizeof(char) * INET_ADDRSTRLEN)))
		return NULL;
	tmp = (struct sockaddr_in *)res->ai_addr;
	if (!inet_ntop(res->ai_family, &tmp->sin_addr, addr, INET_ADDRSTRLEN))
		return NULL;
	return addr;
}

struct udphdr   *set_packet(struct udphdr *packet, int ttl, t_options opt)
{
    (void)ttl;
    packet->uh_sport = htons(opt.sport);
    packet->uh_dport = htons(opt.dport + nb_packet);
    packet->uh_ulen = htons(PACKET_SIZE + sizeof(struct udphdr));
    packet->uh_sum = 0;
    packet->uh_sum = checksum(packet, sizeof(struct udphdr));
    return packet;
}

int    set_and_send_packets(int fd, int ttl, struct sockaddr server, t_options opt)
{
    ssize_t             res;
    void                *udp;
    char                *tmp;
    struct sockaddr_in  *port;
    
    port = (struct sockaddr_in *)&server;
    port->sin_port = htons(opt.dport + nb_packet);
    udp = ft_memalloc(sizeof(struct udphdr) + PACKET_SIZE);    
    if (!udp)
        return -1;
    tmp = (char *)udp + sizeof(struct udphdr);
    ft_memset(tmp, '\0', PACKET_SIZE * sizeof(char));
    tmp[PACKET_SIZE] = 0;
    udp = set_packet((struct udphdr *)udp, ttl, opt);
    if (opt.verbose)
        debug_udp(udp);
    timestamp.dport = opt.dport + nb_packet;
    timestamp.start = get_time_now();
    res = sendto(fd, udp, sizeof(struct udphdr) + PACKET_SIZE, 0, &server, sizeof(server));
    free(udp);
    return res;
}

void    recv_packet(int fd, bool verbose)
{
    void                *buf;
    socklen_t           len;
    ssize_t             res;
    struct sockaddr_in  recep;
    uint32_t            size;

    size = sizeof(struct iphdr) + sizeof(struct icmphdr) + PACKET_SIZE;
    buf = ft_memalloc(size);
    if (!buf)
    {
        printf(" * ");
        return ;
    }
    res = recvfrom(fd, buf, size, 0, (struct sockaddr *)&recep, &len);
    if (res == -1 && !buf)
    {
        printf(" * ");
        free(buf);
        return ;
    }
    timestamp.end = get_time_now();
    if (verbose)
    {
        debug_sockaddr(&recep);
        debug_ip((struct iphdr *)buf);
        debug_icmp((struct icmphdr *)((char *)buf + sizeof(struct iphdr)));
        debug_ip((struct iphdr *)((char *)buf + sizeof(struct iphdr) + sizeof(struct icmphdr)));
        debug_udp((struct udphdr *)((char *)buf + (2 * sizeof(struct iphdr)) + sizeof(struct icmphdr)));
    }
    res = check_ip_hdr((struct iphdr *)buf);
    if (res)
    {
        printf(" * ");
        free(buf);
        return ;
    }
    res = check_icmp_hdr((struct icmphdr *)((char *)buf + sizeof(struct iphdr)));
    if (res)
        print_unreachable(res);
    else
        print_ttl_stats(buf);
    free(buf);
}

void    ft_traceroute(t_args args)
{
    char                *ipv4;
    int                 fd_send;
    int                 fd_recv;

    ipv4 = resolve_dns(args.addr);
    fd_send = create_send_socket();
    fd_recv = create_recv_socket(args.opt);
    if (!ipv4 || fd_send == -1 || fd_recv == -1)
    {
        printf("socket error\n");
        return ;
    }
    print_initial_state(args, ipv4);
    for (int ttl = 1; ttl <= args.opt.hops_max && !finish; ttl++)
    {
        printf(" %d ", ttl);
        ft_memset(&timestamp, 0, sizeof(t_timestamp));
        if (set_udp_socket(fd_send, ttl) != -1)
        {
            print_server_ip = false;
            for (int i = 0; i < args.opt.nb_probe; i++)
            {
                if (set_and_send_packets(fd_send, ttl, *args.addr->ai_addr, args.opt) != -1)
                {
                    recv_packet(fd_recv, args.opt.verbose);
                    ++nb_packet;
                }
                else
                {
                    if (ttl == 1)
                    {
                        printf("traceroute: findsaddr: write: No such process\n");
                        finish = true;
                        break ;
                    }
                    printf(" traceroute: sendto: Can't assign requested address\ntraceroute: wrote %s %zu chars, ret=-1\n",
                        ipv4, sizeof(struct iphdr) + sizeof(struct udphdr) + PACKET_SIZE);
                }
            }
        }
        else
        {
            printf(" * ");
            ft_usleep(1000 * 1000 * args.opt.time);
        }
        printf("\n");
        if (args.opt.verbose)
            printf("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
    }
    close(fd_send);
    close(fd_recv);
    free(ipv4);
}
