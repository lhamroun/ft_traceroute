#include "ft_traceroute.h"

extern bool finish;

int     check_ip_hdr(struct iphdr *ip)
{
    if (ip->ihl != 5)
        return -1;
    if (ip->version != IPV4)
        return -2;
    if (ip->protocol != ICMP)
        return -3;
    if (!ip->saddr)
        return -4;
    if (!ip->daddr)
        return -5;
    return 0;
}

int     check_icmp_hdr(struct icmphdr *icmp)
{
    if (icmp->type == ICMP_TIME_EXCEEDED && icmp->code == ICMP_EXC_TTL)
        return 0;
    if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_PORT_UNREACH)
    {
        finish = true;
        return 0;
    }
    if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_HOST_UNREACH) // !H
        return -1;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_NET_UNREACH) //!N
        return -2;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_PROT_UNREACH) // !P
        return -3;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_SR_FAILED) // !S
        return -4;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_NET_UNKNOWN) // !U
        return -5;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_HOST_UNKNOWN) // !W
        return -6;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_NET_ANO) // !I
        return -7;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_HOST_ANO) // !A
        return -8;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_HOST_ISOLATED) // !Z
        return -9;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_NET_UNR_TOS) // !Q
        return -10;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_HOST_UNR_TOS) // !T
        return -11;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_PKT_FILTERED) // !X
        return -12;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_PREC_VIOLATION) // !V
        return -13;
    else if (icmp->type == ICMP_DEST_UNREACH && icmp->code == ICMP_PREC_CUTOFF) // !C
        return -14;
    return -15;
}

unsigned short	checksum(void *b, int len)
{
	unsigned short	*buf;
	unsigned int	sum;
	unsigned short	result;

	buf = b;
	sum = 0;
	for (sum = 0; len > 1; len -= 2)
		sum += *buf++;
	if (len == 1)
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return result;
}