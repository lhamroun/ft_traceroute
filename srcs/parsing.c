#include "ft_traceroute.h"

/**
**      check if the ip address is valid
**      parms:
**          addr -> ipv4 or dns address
**      return:
**          struct addrinfo containing the ip infos
**/
struct addrinfo *is_ip_addr(char *addr)
{
	struct addrinfo		hints;
    struct addrinfo		*server;

	ft_memset(&hints, 0, sizeof(struct addrinfo));
    server = NULL;
	hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_RAW;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = AI_CANONNAME;
	if (getaddrinfo(addr, NULL, &hints, &server) != 0)
		return NULL;
    return server;
}

t_args  set_default_args(void)
{
    t_args  args;

    ft_memset(&args, 0, sizeof(t_args));
    args.opt.helper = false;
    args.opt.verbose = false;
    args.opt.hops_max = DEFAULT_HOPS;
    args.opt.nb_probe = DEFAULT_NB_PROBES;
    args.opt.time = DEFAULT_TIMEOUT;
    args.opt.sport = SRC_PORT;
    args.opt.dport = DST_PORT;
    args.opt.type = UDP;
    return args;
}

bool    check_args_for_parse(char *str)
{
    if (ft_strcmp(str, "-m")
        && ft_strcmp(str, "-p")
        && ft_strcmp(str, "-q")
        && ft_strcmp(str, "-w"))
        return true;
    return false;
}

t_args  parsing(int ac, char **av)
{
    void            *tmp;
    t_args          args;
    t_options_check opt_check;

    ft_memset(&opt_check, 0, sizeof(t_options_check));
    args = set_default_args();
    for (int i = 0; i < ac; i++)
    {
        args.addr = NULL;
        if ((i == 0 && check_args_for_parse(av[i - 1]))
            || (check_args_for_parse(av[i - 1]) && check_args_for_parse(av[i])))
        {
            args.addr = is_ip_addr(av[i]);
        }
        if (args.addr)
        {
            tmp = args.addr;
            ++opt_check.nnb_ip;
            args.ip = av[i];
        }
        else if (!ft_strcmp(av[i], "--help"))
        {
            ++opt_check.nhelper;
            args.opt.helper = true;
        }
        else if (!ft_strcmp(av[i], "-v")) // verbose for more log than TTL exceed et dest unreach
        {
            ++opt_check.nverbose;
            args.opt.verbose = true;
        }
        else if (!ft_strcmp(av[i], "-m")) // nb hops max
        {
            if (i >= listlen(av) - 1
                || !str_is_number(av[i + 1])
                || ft_atoi(av[i + 1]) < 1
                || ft_atoi(av[i + 1]) > MAX_HOPS)
            {
                printf("traceroute: hops must be 0 < hops < MAX_HOPS\n");
                opt_check.nhelper = 42;
                break ;
            }
            ++opt_check.nhops_max;
            args.opt.hops_max = ft_atoi(av[i + 1]);
            ++i;
        }
        else if (!ft_strcmp(av[i], "-p")) // dest port
        {
            if (i >= listlen(av) - 1
                || !str_is_number(av[i + 1])
                || ft_atoi(av[i + 1]) < 1
                || ft_atoi(av[i + 1]) > USHRT_MAX)
            {
                printf("traceroute: port must be 0 < dport <= 65535\n");
                opt_check.nhelper = 42;
                break ;
            }
            ++opt_check.ndport;
            args.opt.dport = ft_atoi(av[i + 1]);
            ++i;
        }
        else if (!ft_strcmp(av[i], "-q")) // nb probes
        {
            if (i >= listlen(av) - 1
                || !str_is_number(av[i + 1])
                || ft_atoi(av[i + 1]) < 1
                || ft_atoi(av[i + 1]) > MAX_PROBES)
            {
                printf("traceroute: nprobes must be : 0 < nprobes < MAX_PROBES\n");
                opt_check.nhelper = 42;
                break ;
            }
            ++opt_check.nnb_probe;
            args.opt.nb_probe = ft_atoi(av[i + 1]);
            ++i;
        }
        else if (!ft_strcmp(av[i], "-w")) // time to wait before timeout probe
        {
            if (i >= listlen(av) - 1
                || !str_is_number(av[i + 1])
                || ft_atoi(av[i + 1]) < 1
                || ft_atoi(av[i + 1]) > MAX_TIMEOUT)
            {
                printf("traceroute: timeout must be : 0 < timeout < MAX_TIMEOUT\n");
                opt_check.nhelper = 42;
                break ;
            }
            ++opt_check.ntime;
            args.opt.time = ft_atoi(av[i + 1]);
            ++i;
        }
        else // unknown option or hostname
        {
            printf("traceroute: unknown host %s\n", av[i]);
            opt_check.nhelper = 42;
            break ;
        }
    }
    if (opt_check.nhelper > 1
        || opt_check.nverbose > 1
        || opt_check.nhops_max > 1
        || opt_check.nnb_probe > 1
        || opt_check.ntime > 1
        || opt_check.ndport > 1
        || opt_check.nnb_ip > 1)
    {
        args.opt.helper = true;
    }
    args.addr = tmp;
    return args;
}